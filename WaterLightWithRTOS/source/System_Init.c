#include "System_Init.h"
#include "fsl_port.h"
#include "fsl_pit.h"
#include "fsl_debug_console.h"

static void init_leds(void)
{
    /* Define the init structure for the output LED pin*/
    gpio_pin_config_t led_config = {
        kGPIO_DigitalOutput, 1,
    };
    
    GPIO_PinInit(BOARD_INITPINS_LED_RED_GPIO, BOARD_INITPINS_LED_RED_PIN, &led_config);
    GPIO_PinInit(BOARD_INITPINS_LED_BLUE_GPIO, BOARD_INITPINS_LED_BLUE_PIN, &led_config);
    GPIO_PinInit(BOARD_INITPINS_LED_GREEN_GPIO, BOARD_INITPINS_LED_GREEN_PIN, &led_config);
    
    return;
}

static void init_pit(void)
{
    pit_config_t pitConfig;
    PIT_GetDefaultConfig(&pitConfig);
    PIT_Init(PIT, &pitConfig);
    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(10000U, CLOCK_GetFreq(kCLOCK_BusClk)));
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
    EnableIRQ(PIT0_IRQn); 
}
        
void init_system(void)
{
    /*
        PS: 这里太坑了！
        根据FreeRTOS手册，启用断言的话会有个函数
        portASSERT_IF_INTERRUPT_PRIORITY_INVALID();来检查当前中断的优先级，
        优先级不得低于portFIRST_USER_INTERRUPT_NUMBER设置的值，系统设置默认值为16，
        根据说明，PriorityGrouping 需设置为 0 
        Priority设置要大于2，而且必须要在调度器函数vTaskStartScheduler()之前使用
        设置完了才能在中断中使用各种xxxxxFromISR函数，否则会一~直~卡~在~断~言~处~！！
    */
    NVIC_SetPriorityGrouping( 0 );
    NVIC_SetPriority(PIT0_IRQn , 2 );
    init_leds();
    init_pit();
    
    return;
}