#ifndef SYSTEM_INIT_H
#define SYSTEM_INIT_H

#include "pin_mux.h"
#include "fsl_pit.h"
#include "fsl_gpio.h"

#define LED_RED_ON() \
        GPIO_PortClear(BOARD_INITPINS_LED_RED_GPIO,1 << BOARD_INITPINS_LED_RED_PIN)
#define LED_BLUE_ON() \
        GPIO_PortClear(BOARD_INITPINS_LED_BLUE_GPIO,1 << BOARD_INITPINS_LED_BLUE_PIN)
#define LED_GREEN_ON() \
        GPIO_PortClear(BOARD_INITPINS_LED_GREEN_GPIO,1 << BOARD_INITPINS_LED_GREEN_PIN)
#define LED_YELLOW_ON() \
        LED_RED_ON();LED_GREEN_ON()
            
            
#define LED_RED_OFF() \
        GPIO_PortSet(BOARD_INITPINS_LED_RED_GPIO,1 << BOARD_INITPINS_LED_RED_PIN)
#define LED_BLUE_OFF() \
        GPIO_PortSet(BOARD_INITPINS_LED_BLUE_GPIO,1 << BOARD_INITPINS_LED_BLUE_PIN)
#define LED_GREEN_OFF() \
        GPIO_PortSet(BOARD_INITPINS_LED_GREEN_GPIO,1 << BOARD_INITPINS_LED_GREEN_PIN)
#define LED_YELLOW_OFF() \
        LED_RED_OFF();LED_GREEN_OFF()
            
            
#define LED_RED_TOGGLE() \
        GPIO_PortToggle(BOARD_INITPINS_LED_RED_GPIO,1 << BOARD_INITPINS_LED_RED_PIN)
#define LED_BLUE_TOGGLE() \
        GPIO_PortToggle(BOARD_INITPINS_LED_BLUE_GPIO,1 << BOARD_INITPINS_LED_BLUE_PIN)
#define LED_GREEN_TOGGLE() \
        GPIO_PortToggle(BOARD_INITPINS_LED_GREEN_GPIO,1 << BOARD_INITPINS_LED_GREEN_PIN)            
#define LED_YELLOW_TOGGLE() \
        LED_RED_TOGGLE();LED_GREEN_TOGGLE()

            
extern void init_system(void);

#endif