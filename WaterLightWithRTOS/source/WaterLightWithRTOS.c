/*System includes.*/
#include <stdio.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"

#include "System_Init.h"
#include "SystemLight.h"

/*******************************************************************************
* Globals
******************************************************************************/
/* Logger queue handle */
static QueueHandle_t vsEventQueue = NULL;

/* Application API */
static void eventDistribution_task( void* pvParameters );


/*****************************************************************************
 函 数 名  : ErrorHandle
 功能描述  : 错误返回值处理函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2018年11月10日
    作    者   : 陈星志
    修改内容   : 新生成函数

*****************************************************************************/
void ErrorHandle( void )
{
    PRINTF( "Function Run Error!\n" );
    while( 1 )
    {
        ;
    }
}

/*****************************************************************************
 函 数 名  : main
 功能描述  : 主函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2018年11月10日
    作    者   : 陈星志
    修改内容   : 新生成函数

*****************************************************************************/
int main( void )
{
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    init_system();
    if( xTaskCreate( eventDistribution_task,
                     "Event Distribution Task",
                     1024,
                     NULL,
                     tskIDLE_PRIORITY + 2,
                     NULL ) != pdPASS )
    {
        ErrorHandle();
    }

    vTaskStartScheduler();
    while( 1 )
    {
        ;
    }
}

/*****************************************************************************
 函 数 名  : eventDistribution_task
 功能描述  : 任务分发函数，接受VS发送的事件，将事件�-
                 �行处理
 输入参数  : void* pvParameters
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2018年11月10日
    作    者   : 陈星志
    修改内容   : 新生成函数

*****************************************************************************/
void eventDistribution_task( void* pvParameters )
{
    const TickType_t taskDelayTime = 20 / portTICK_PERIOD_MS;
    SEM_EVENT_TYPE eventNo = SE_RESET;
    /* 初始化 VS System. */
    SystemLightVSInitAll();
    vsEventQueue = xQueueCreate( 20, sizeof( SEM_EVENT_TYPE ) );
    if( xQueueSend( vsEventQueue, &eventNo, 0 ) == errQUEUE_FULL )
    {
        ErrorHandle();
    }
    /* 启动发送事件用的定时器 */
    PIT_StartTimer(PIT, kPIT_Chnl_0);
    while( 1 )
    {
        if( xQueueReceive( vsEventQueue, &eventNo, 0 ) == pdTRUE )
        {
            if( SystemLightVSDeduct( eventNo ) != SES_OKAY )
            {
                ErrorHandle();
            }
        }
        vTaskDelay( taskDelayTime );
    }
}

/*****************************************************************************
 函 数 名  : PIT0_IRQHandler
 功能描述  : PIT0 中断服务函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2018年11月10日
    作    者   : 陈星志
    修改内容   : 新生成函数

*****************************************************************************/
void PIT0_IRQHandler( void )
{
    static uint16_t uTimerTick = 0;
    SEM_EVENT_TYPE eventNo = evOneSecond;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    PIT_ClearStatusFlags( PIT, kPIT_Chnl_0, kPIT_TimerFlag );
    if( uTimerTick++ >= 100 )
    {
        uTimerTick = 0;
        xQueueSendFromISR( vsEventQueue, &( eventNo ), &xHigherPriorityTaskWoken );
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}