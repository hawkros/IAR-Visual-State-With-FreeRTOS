/*
 * Id:        SystemLight.c
 *
 * Function:  Contains all API functions.
 *
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "SystemLight.h"
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)


#include <stdint.h>
#endif


enum SystemLightDeductIterationEnum
{
  kDIOriginalEvent = 0U,
  kDITakeSignalOut = 5U
};


/*
 * Used internally in the API
 */
enum SystemLightSEMStateEnum
{
  STATE_SEM_NOT_INITIALIZED = 0U,
  STATE_SEM_INITIALIZED     = 1U,
  STATE_SEM_PREPARE         = 2U,
  STATE_SEM_OKAY            = 3U
};


/*
 * SEM Library Datatype Definition.
 */
struct SystemLightSEMDATA
{
  enum SystemLightSEMStateEnum                  State;
  SEM_EVENT_TYPE                                EventNo;
  SEM_STATE_TYPE                                CSV[VS_NOF_STATE_MACHINES];
  SEM_STATE_TYPE                                WSV[VS_NOF_STATE_MACHINES];
  SEM_EVENT_TYPE                                SQueue[10];
  SEM_SIGNAL_QUEUE_TYPE                         SPut;
  SEM_SIGNAL_QUEUE_TYPE                         SGet;
  SEM_SIGNAL_QUEUE_TYPE                         SUsed;
};


/*
 * VS System Datatype Definition.
 */
struct VSDATASystemLight
{
  uint8_t        StateMachineIndex[0X008];
  uint8_t        RuleData[0X05e];
  uint8_t        RuleIndex[0X00c];
  uint8_t        RuleTableIndex[0X006];
};


/*
 * SEM Library data definition.
 */
struct SystemLightSEMDATA SEMSystemLight;


/*
 * VS System data definition and initialization. Rule data format number: 1
 */
struct VSDATASystemLight const SystemLight = 
{
  {
    0x00U, 0x00U, 0x00U, 0x01U, 0x01U, 0x02U, 0x02U, 0x02U
  },
  {
    0x00U, 0x30U, 0x11U, 0x02U, 0x03U, 0x05U, 0x02U, 0x01U, 
    0x01U, 0x10U, 0x02U, 0x00U, 0x02U, 0x02U, 0x01U, 0x01U, 
    0x10U, 0x02U, 0x01U, 0x00U, 0x04U, 0x03U, 0x01U, 0x20U, 
    0x02U, 0x02U, 0x01U, 0x03U, 0x00U, 0x05U, 0x02U, 0x20U, 
    0x01U, 0x01U, 0x03U, 0x01U, 0x04U, 0x04U, 0x02U, 0x20U, 
    0x01U, 0x01U, 0x04U, 0x01U, 0x03U, 0x05U, 0x01U, 0x11U, 
    0x11U, 0x05U, 0x00U, 0x06U, 0x04U, 0x06U, 0x01U, 0x11U, 
    0x11U, 0x05U, 0x01U, 0x05U, 0x02U, 0x07U, 0x01U, 0x11U, 
    0x11U, 0x06U, 0x02U, 0x07U, 0x03U, 0x06U, 0x01U, 0x11U, 
    0x11U, 0x06U, 0x03U, 0x06U, 0x04U, 0x07U, 0x01U, 0x11U, 
    0x11U, 0x07U, 0x04U, 0x05U, 0x02U, 0x06U, 0x01U, 0x11U, 
    0x11U, 0x07U, 0x05U, 0x07U, 0x03U, 0x07U
  },
  {
    0x00U, 0x1EU, 0x26U, 0x2EU, 0x36U, 0x3EU, 0x46U, 0x4EU, 
    0x56U, 0x08U, 0x0FU, 0x16U
  },
  {
    0x00U, 0x01U, 0x09U, 0x0AU, 0x0BU, 0x0CU
  }
};


/* Core model logic struct name */
#define VS SystemLight


/* SEM data struct name */
#define SEM SEMSystemLight


/* Signal queue function prototypes */
static SEM_EVENT_TYPE SEM_SignalQueueGet (void);
static VSResult SEM_SignalQueuePut (SEM_EVENT_TYPE SignalNo);


/*
 * Guard expression type definition
 */
typedef _Bool (* SystemLightVS_GUARDEXPR_TYPE) (void);


static void SystemLightDeductChangeState (void);


static VSResult SystemLightSEM_GetOutput (void);


/*
 * VS System Internal Variable Definition(s).
 */

static int g_iOneSecondCounter = 0;


union SystemLightVBDATA
{
  struct
  {
    int              intVar[0X00001];
  } DB1;
};


/*
 * VS System Variable Buffer Variable Declaration.
 */
static union SystemLightVBDATA SystemLightVBVar;


static void SystemLightBufferVariables (SEM_EVENT_TYPE EventNo);


static void SystemLightBufferVariables (SEM_EVENT_TYPE EventNo)
{
  switch (EventNo)
  {
  case 1:
    SystemLightVBVar.DB1.intVar[0] = g_iOneSecondCounter;
    break;

  default:
    break;
  }
}


/*
 * VS Deduct Function.
 */
VSResult SystemLightVSDeduct (SEM_EVENT_TYPE EventNo)
{
  VSResult cc;
  if (SEMSystemLight.State == STATE_SEM_NOT_INITIALIZED)
  {
    return SES_NOT_INITIALIZED;
  }
  if (2U <= EventNo)
  {
    return (SES_RANGE_ERR);
  }
  SEMSystemLight.EventNo = EventNo;
  SEMSystemLight.State = STATE_SEM_PREPARE;
  SystemLightBufferVariables(EventNo);
  cc = SystemLightSEM_GetOutput();
  if (cc == SES_OKAY)
  {
    SystemLightDeductChangeState();
    SEMSystemLight.State = STATE_SEM_INITIALIZED;
  }
  return cc;
}


/*
 * Guard Expression Functions.
 */

static _Bool SystemLightVSGuard_0 (void);
static _Bool SystemLightVSGuard_0 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] == iTimeGreenOn);
}

static _Bool SystemLightVSGuard_1 (void);
static _Bool SystemLightVSGuard_1 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] < iTimeGreenOn);
}

static _Bool SystemLightVSGuard_2 (void);
static _Bool SystemLightVSGuard_2 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] == iTimeYellowBlinkTime);
}

static _Bool SystemLightVSGuard_3 (void);
static _Bool SystemLightVSGuard_3 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] < iTimeYellowBlinkTime);
}

static _Bool SystemLightVSGuard_4 (void);
static _Bool SystemLightVSGuard_4 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] == iTimeRedOn);
}

static _Bool SystemLightVSGuard_5 (void);
static _Bool SystemLightVSGuard_5 (void)
{
  return (_Bool)(SystemLightVBVar.DB1.intVar[0] < iTimeRedOn);
}


/*
 * Guard Expression Pointer Table.
 */
SystemLightVS_GUARDEXPR_TYPE const SystemLightVSGuard[6] = 
{
  &SystemLightVSGuard_0,
  &SystemLightVSGuard_1,
  &SystemLightVSGuard_2,
  &SystemLightVSGuard_3,
  &SystemLightVSGuard_4,
  &SystemLightVSGuard_5
};


/*
 * Action Expression Function Prototypes.
 */
static void SystemLightVSAction_6 (void);

static void SystemLightVSAction_7 (void);


/*
 * Action expression type definition
 */
typedef void (* SystemLightVS_ACTIONEXPR_TYPE) (void);


/*
 * Action Expression Pointer Table Declaration.
 */
static SystemLightVS_ACTIONEXPR_TYPE const SystemLightVSAction[8];


/*
 * Action Expression Functions.
 */
static void SystemLightVSAction_6 (void)
{
  g_iOneSecondCounter = 0;
}
static void SystemLightVSAction_7 (void)
{
  g_iOneSecondCounter = (g_iOneSecondCounter + 1);
}


/*
 * Action Expression Pointer Table Definition.
 */
static SystemLightVS_ACTIONEXPR_TYPE const SystemLightVSAction[8] = 
{
  &led_green_off,
  &led_green_on,
  &led_red_off,
  &led_red_on,
  &led_yellow_off,
  &led_yellow_on,
  &SystemLightVSAction_6,
  &SystemLightVSAction_7
};


void SystemLightSEM_InitSignalQueue (void)
{
  SEM.SPut = 0U;
  SEM.SGet = 0U;
  SEM.SUsed = 0U;
}


static VSResult SEM_SignalQueuePut (SEM_EVENT_TYPE SignalNo)
{
  VSResult result;

  if (SEM.SUsed == 10U)
  {
    result = SES_SIGNAL_QUEUE_FULL;
  }
  else
  {
    ++SEM.SUsed;
    SEM.SQueue[SEM.SPut] = SignalNo;
    ++SEM.SPut;
    if (SEM.SPut == 10U)
    {
      SEM.SPut = 0U;
    }
    result = SES_OKAY;
  }
  return (result);
}


static SEM_EVENT_TYPE SEM_SignalQueueGet (void)
{
  SEM_EVENT_TYPE SignalNo = EVENT_UNDEFINED;

  if (SEM.SUsed != 0U)
  {
    SEM.SUsed--;
    SignalNo = SEM.SQueue[SEM.SGet];
    ++SEM.SGet;
    if (SEM.SGet == 10U)
    {
      SEM.SGet = 0U;
    }
  }
  return (SignalNo);
}





void SystemLightVSInitAll (void)
{
  SystemLightSEM_Init();
  SystemLightSEM_InitSignalQueue();
}


void SystemLightSEM_Init (void)
{
  {
    SEM_STATE_MACHINE_TYPE i;
    for (i = 0U; i < 3U; ++i)
    {
      SEM.WSV[i] = STATE_UNDEFINED;
      SEM.CSV[i] = STATE_UNDEFINED;
    }
  }
  SEM.State = STATE_SEM_INITIALIZED;
}


static void SystemLightDeductChangeState (void)
{
  SEM_STATE_MACHINE_TYPE i;
  for (i = 0U; i < 3U; ++i)
  {
    if (SEM.WSV[i] != STATE_UNDEFINED)
    {
      SEM.CSV[i] = SEM.WSV[i];
      SEM.WSV[i] = STATE_UNDEFINED;
    }
  }
}


static VSResult SystemLightSEM_GetOutput (void)
{
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
  uint_fast8_t DIt = kDIOriginalEvent;
  uint_fast8_t iFirstR;
  uint_fast8_t iLastR;
#else
  uint8_t DIt = kDIOriginalEvent;
  SEM_RULE_TABLE_INDEX_TYPE iFirstR;
  SEM_RULE_TABLE_INDEX_TYPE iLastR;
#endif
  for(;;)
  {
    switch (SEM.State)
    {
    case STATE_SEM_PREPARE:
      switch (DIt)
      {
      case kDIOriginalEvent:
        DIt = kDITakeSignalOut;
        break;

      case kDITakeSignalOut:
        SEMSystemLight.EventNo = SEM_SignalQueueGet();
        if (SEMSystemLight.EventNo == EVENT_UNDEFINED)
        {
          SEMSystemLight.State = STATE_SEM_OKAY;
          return (SES_OKAY);
        }
        DIt = kDITakeSignalOut;
        SystemLightBufferVariables(SEMSystemLight.EventNo);
        SystemLightDeductChangeState();
        break;

      }

      iFirstR = VS.RuleTableIndex[SEM.EventNo];
      iLastR = VS.RuleTableIndex[SEM.EventNo + 1U];
      while (iFirstR < iLastR)
      {
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
        uint_fast8_t i;
        uint_fast8_t nNo;
        uint_fast8_t nPos;
        uint_fast8_t nNxt;
        uint_fast8_t nSignal;
        uint_fast8_t nGuard;
        uint_fast8_t iRI;
        uint_fast8_t nAction;
#else
        SEM_INTERNAL_TYPE i;
        uint8_t nNo;
        uint8_t nPos;
        uint8_t nNxt;
        uint8_t nSignal;
        uint8_t nGuard;
        SEM_RULE_INDEX_TYPE iRI;
        uint8_t nAction;
#endif

        iRI = VS.RuleIndex[iFirstR];
        ++iFirstR;
        i = VS.RuleData[iRI];
        ++iRI;
        nPos = (unsigned char)(i & 0x0FU);
        i = VS.RuleData[iRI];
        ++iRI;
        nGuard = (unsigned char)(i & 0x0FU);
        nNxt = (unsigned char)(i >> 4U);
        i = VS.RuleData[iRI];
        ++iRI;
        nAction = (unsigned char)(i & 0x0FU);
        nSignal = (unsigned char)(i >> 4U);

        for (nNo = 0U; nNo < nPos; ++nNo)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE)(VS.RuleData[iRI]);
          if (sa != SEM.CSV[VS.StateMachineIndex[sa]])
          {
            goto NextRule;
          }
          else
          {
            ++iRI;
          }
        }

        if (nGuard != 0U)
        {
          for (nNo = 0U; nNo < nGuard; ++nNo)
          {
            if ((*SystemLightVSGuard[VS.RuleData[iRI]])() != 0U)
            {
              ++iRI;
            }
            else
            {
              goto NextRule;
            }
          }
        }

        for (nNo = 0U; nNo < nNxt; ++nNo)
        {
          SEM_STATE_TYPE sa;
          sa = (SEM_STATE_TYPE)(VS.RuleData[iRI]);
          ++iRI;
          i = VS.StateMachineIndex[sa];
          if (SEM.WSV[i] == STATE_UNDEFINED)
          {
            SEM.WSV[i] = sa;
          }
          else if (SEM.WSV[i] != sa)
          {
            return (SES_CONTRADICTION);
          }
        }
        if (nSignal != 0U)
        {
          for (nNo = 0U; nNo < nSignal; ++nNo)
          {
            i = VS.RuleData[iRI];
            ++iRI;
            if (SEM_SignalQueuePut((SEM_EVENT_TYPE)(i)) == SES_SIGNAL_QUEUE_FULL)
            {
              return (SES_SIGNAL_QUEUE_FULL);
            }
          }
        }
        while (nAction != 0U)
        {
          SEM_ACTION_EXPRESSION_TYPE actionNo;
          actionNo = (SEM_ACTION_EXPRESSION_TYPE)(VS.RuleData[iRI]);
          ++iRI;
          nAction--;
          (*SystemLightVSAction[actionNo])();
        }
NextRule:
        ;
      }
      break;

    case STATE_SEM_OKAY:
      return (SES_OKAY);

    default:
      return (SES_EMPTY);
    }
  }
}


VSResult SystemLightVSElementName (IdentifierType IdentType, SEM_EXPLANATION_TYPE IdentNo, char const * * Text)
{
  VSResult ret = SES_OKAY;
  switch (IdentType)
  {
  default:
    ret = SES_TYPE_ERR;
    break;
  }
  return ret;
}


VSResult SystemLightVSElementExpl (IdentifierType IdentType, SEM_EXPLANATION_TYPE IdentNo, char const * * Text)
{
  VSResult ret = SES_OKAY;
  switch (IdentType)
  {
  default:
    ret = SES_TYPE_ERR;
    break;
  }
  return ret;
}


VSResult SystemLightSEM_State (SEM_STATE_MACHINE_TYPE StateMachineNo,
  SEM_STATE_TYPE *StateNo)
{
  if (3U <= StateMachineNo)
  {
    return (SES_RANGE_ERR);
  }
  *StateNo = SEM.CSV[StateMachineNo];
  return (SES_FOUND);
}
